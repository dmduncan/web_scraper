import requests
import requests_ftp
import logging


def get_all_tickers():
    """Get all tickers listed on US market"""

    exchange = 'nasdaq'
    file_data = pull_file(exchange)
    nasdaq_stock_tickers, nasdaq_etf_tickers = parse_tickers(file_data, exchange)

    exchange = 'other'
    file_data = pull_file(exchange)
    other_stock_tickers, other_etf_tickers = parse_tickers(file_data, exchange)

    stock_tickers = nasdaq_stock_tickers + other_stock_tickers
    etf_tickers = nasdaq_etf_tickers + other_etf_tickers

    return stock_tickers, etf_tickers


def pull_file(exchange):
    """Pull list of securities listed on the US market"""

    if exchange is 'nasdaq':
        # tickers listed on the NASDAQ
        file = "ftp://ftp.nasdaqtrader.com/SymbolDirectory/nasdaqlisted.txt"
    elif exchange is 'other':
        # rest of the tickers listed on US markets
        file = "ftp://ftp.nasdaqtrader.com/SymbolDirectory/otherlisted.txt"
    else:
        return

    requests_ftp.monkeypatch_session()
    session = requests.Session()

    resp = session.get(file)
    return resp.content


def parse_tickers(file_data, exchange):
    '''Parses the NASDAQ Ticker Info file and returns list of all tickers in the file'''

    if exchange is 'nasdaq':
        etf_index = 6
    elif exchange is 'other':
        etf_index = 4
    else:
        return

    stocks = []
    etfs = []

    # break out each line by the character return followed by the newline statement
    lines = file_data.split(b'\r\n')
    for line in lines:

        # convert the byte literals to strings
        line = line.decode("utf-8")

        # skip if there is an empty line (some blank lines occasionally at the end of the file)
        if not line:
            continue

        sections = line.split('|')
        ticker = sections[0].strip()

        # remove appended ticker info (like class c shares etc)
        if '.' in ticker:
            ticker = ticker[:ticker.index('.')]
        if '-' in ticker:
            ticker = ticker[:ticker.index('-')]
        if '$' in ticker:
            ticker = ticker[:ticker.index('$')]

        # Don't take the first line (table headers)
        if 'Symbol' in ticker or 'Creation Time' in ticker:
            continue

        if sections[etf_index] is 'Y':
            etfs.append(ticker)
        else:
            stocks.append(ticker)

    # convert list to set to get distinct values, then convert back
    # to a list to return lists with no duplicate tickers
    return list(set(stocks)), list(set(etfs))


def update_lists(online_stock_tickers, online_etf_tickers):
    """Update both the internal stock and etf ticker lists"""

    # read in the stock tickers stored on our local system
    with open('../tickers_list/stock_tickers.txt', 'r') as stock_tickers_reader:
        internal_stock_tickers = []
        for ticker in stock_tickers_reader.readlines():
            internal_stock_tickers.append(ticker.strip())

    # read in the etf tickers stored on our local system
    with open('../tickers_list/etf_tickers.txt', 'r') as etf_tickers_reader:
        internal_etf_tickers = []
        for ticker in etf_tickers_reader.readlines():
            internal_etf_tickers.append(ticker.strip())

    update_list(internal_stock_tickers, online_stock_tickers)
    update_list(internal_etf_tickers, online_etf_tickers)

    # Write our updated internal stock ticker list to file
    with open('../tickers_list/stock_tickers.txt', 'w') as stock_tickers_writer:
        for ticker in sorted(internal_stock_tickers):
            stock_tickers_writer.write(ticker + '\n')

    # Write our updated internal etf ticker list to file
    with open('../tickers_list/etf_tickers.txt', 'w') as etf_tickers_writer:
        for ticker in sorted(internal_etf_tickers):
            etf_tickers_writer.write(ticker + '\n')


def update_list(internal_tickers, online_tickers):
    '''Updates the internal ticker list to the one scraped from online and logs the changes'''

    tickers_removed = []
    tickers_added = []

    # add any new tickers not in our internal list
    for ticker in online_tickers:
        if ticker not in internal_tickers:
            tickers_added.append(ticker)
            internal_tickers.append(ticker)

    # remove any old tickers from our internal list that are not listed anymore or symbol may have changed
    # we create a copy of internal_tickers because we cant delete from a list we are iterating over
    for ticker in internal_tickers[:]:
        if ticker not in online_tickers:
            tickers_removed.append(ticker)
            internal_tickers.remove(ticker)

    for ticker in tickers_added:
        logging.info('Ticker {0} not in our internal list. Adding Ticker.'.format(ticker))

    for ticker in tickers_removed:
        logging.info('Ticker {0} not in the online list. Removing Ticker'.format(ticker))


def main():

    logging.basicConfig(filename='../tickers_list/pull_tickers.log', level=logging.DEBUG,
                        format='%(asctime)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('Started')# + str(datetime.today()))

    stock_tickers, etf_tickers = get_all_tickers()
    update_lists(stock_tickers, etf_tickers)

    logging.info('Finished')



if __name__ == '__main__':
    main()