from dev.scrape_nasdaq_dozen import *

def nasdaq_dozen_test(ticker):

    file = codecs.open('output\\nasdaq_dozen.txt', 'w', encoding='utf-8')
    # nasdaq_dozen_tests = OrderedDict()
    #
    # nasdaq_dozen_tests['revenue'] = revenue_test(ticker)
    # nasdaq_dozen_tests['earnings_per_share'] = eps_test(ticker)
    # nasdaq_dozen_tests['return_on_equity'] = roe_test(ticker)
    # nasdaq_dozen_tests['earnings_surprise'] = earnings_surprise_test(ticker)
    # nasdaq_dozen_tests['earnings_forecast'] = earnings_forecast_test(ticker)
    #
    # nasdaq_dozen_tests['earnings_growth'] = earnings_growth_test(ticker)
    # nasdaq_dozen_tests['peg_ratio'] = peg_ratio_test(ticker)
    # nasdaq_dozen_tests['days_to_cover'] = days_to_cover_test(ticker)
    # nasdaq_dozen_tests['insider_trading'] = insider_trading_test(ticker)
    #
    #
    #
    #
    #
    # for key, value in nasdaq_dozen_tests.items():
    #     print(key + ': %s' % value)

    # print()


def revenue_test(ticker):
    """Checks to see if revenue is increasing. First check the annual totals, but if the most recent fiscal year is
    incomplete, then compare the most recent quarter with the same quarter in the previous year."""

    revenue_df = pull_revenue(ticker)
    if revenue_df.empty:
        return False

    curr_fiscal_year_complete = True
    most_recent_quater_loc = 0

    # check to see if the most recent fiscal year is complete and revenue is recorded for each quarter (no blanks).
    # if not complete, record the location most recent quarter
    i = 0
    for quart_rev in revenue_df.iloc[:,1]:
        #print(":".join("{:02x}".format(ord(c)) for c in quart_rev))
        if (not quart_rev) or (quart_rev == chr(0xa0) or (quart_rev == ' ')):
            curr_fiscal_year_complete = False
            most_recent_quater_loc = i-1
        i += 1

    # if all years are complete, check to see if each year the revenue is increasing
    if curr_fiscal_year_complete:
        prev_rev = -99999

        # pull the revenue for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_rev in reversed(revenue_df.iloc[-1,1:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if year_rev == chr(0xa0) or year_rev == 'N/A':
                return False

            year_rev = nasdaq_revenue_conversion(year_rev)
            if year_rev < prev_rev:
                return False

            prev_rev = year_rev

    else:
        prev_rev = -99999

        # pull the revenue for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_rev in reversed(revenue_df.iloc[-1, 2:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if year_rev == chr(0xa0) or year_rev == 'N/A':
                return False

            year_rev = nasdaq_revenue_conversion(year_rev)
            if year_rev < prev_rev:
                return False

            prev_rev = year_rev

        prev_rev = -99999
        # pull the revenue for the last listed fiscal quarter and the revenues from previous years for the same quarter
        for quart_rev in reversed(revenue_df.iloc[most_recent_quater_loc, 1:-1]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if quart_rev == chr(0xa0) or quart_rev == 'N/A':
                return False

            quart_rev = nasdaq_revenue_conversion(quart_rev)
            if quart_rev < prev_rev:
                return False

            prev_rev = quart_rev

    return True


def eps_test(ticker):
    """Checks to see if revenue is increasing. First check the annual totals, but if the most recent fiscal year is
        incomplete, then compare the most recent quarter with the same quarter in the previous year."""

    eps_df = pull_eps(ticker)
    if eps_df.empty:
        return False

    curr_fiscal_year_complete = True
    most_recent_quater_loc = 0

    # check to see if the most recent fiscal year is complete and eps is recorded for each quarter (no blanks).
    # if not complete, record the location most recent quarter
    i = 0
    for quart_eps in eps_df.iloc[:, 1]:
        #print(":".join("{:02x}".format(ord(c)) for c in quart_eps))
        if (not quart_eps) or (quart_eps == chr(0xa0) or (quart_eps == ' ')):
            curr_fiscal_year_complete = False
            most_recent_quater_loc = i - 1
        i += 1

    # # if all years are complete, check to see if each year the eps is increasing
    if curr_fiscal_year_complete:
        prev_eps = -99999

        # pull the eps for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_eps in reversed(eps_df.iloc[-1, 1:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if year_eps == chr(0xa0) or year_eps == 'N/A':
                return False

            year_eps = float(year_eps)

            if year_eps < prev_eps:
                return False

            prev_eps = year_eps

    else:
        prev_eps = -99999

        #pull the eps for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_eps in reversed(eps_df.iloc[-1, 2:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if year_eps == chr(0xa0) or year_eps == 'N/A':
                return False

            year_eps = float(year_eps)

            if year_eps < prev_eps:
                return False

            prev_eps = year_eps

        prev_eps = -99999
        # pull the eps for the last listed fiscal quarter and the eps' from previous years for the same quarter
        for quart_eps in reversed(eps_df.iloc[most_recent_quater_loc, 1:-1]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if quart_eps == chr(0xa0) or quart_eps == 'N/A':
                return False

            # remove the eps release date next to the number and convert it to a float to compare
            quart_eps = float(quart_eps[:quart_eps.index('(')])

            if quart_eps < prev_eps:
                return False

            prev_eps = quart_eps

    return True


def roe_test(ticker):
    """Give ROE a passing score if ROE has been increasing for two consecutive years."""

    financial_ratios_df = pull_financial_ratios(ticker)

    #print(prettify_dataframe(financial_ratios_df))
    # #x = financial_ratios_df.loc["After Tax ROE", "12/31/2015"]
    # x = financial_ratios_df.get_group("After Tax ROE")
    # print(x)

    prev_roe = -99999
    # pull the After Tax Return on Equity (RoE) percentage for each year
    for year_roe in reversed(financial_ratios_df.iloc[-1, 1:]):

        # if the field is blank or is 'N/A' return false
        if year_roe == chr(0xa0) or year_roe == 'N/A':
            return False

        print(year_roe)
        year_roe = float(year_roe[:year_roe.index('%')])
        print(year_roe)

        if year_roe < prev_roe:
            return False

        prev_roe = year_roe

    return True



def analyst_recommendations_test(ticker):
    pass


def earnings_surprise_test(ticker):
    """Takes the surprises dataframe pulled from NASDAQ and rates if the EPS surprise was a pass or fail.
    If all EPS percent surprises for the last 4 quaters are greater than 0, pass, else, fail. """

    surprises_df = pull_quart_earning_surprises(ticker)
    if surprises_df.empty:
        return False

    for value in surprises_df['Percent Surprise']:

        # check to see if one of the columns is blank (empty string)
        if not value:
            continue

        # convert to a decimal and check if it is positive
        if float(value) < 0:
            return False

    return True


def earnings_forecast_test(ticker):
    """Takes the eps forecasts dataframe pulled from NASDAQ and rates if the earnings forecast was a pass or fail.
    If the consensus EPS forecast is increasing each year give it a pass, else fail. """

    eps_forecasts_df = pull_yearly_eps_forecast(ticker)
    if eps_forecasts_df.empty:
        return False

    previous_eps = -99999
    for eps in eps_forecasts_df['Consensus EPS Forecasts']:

        # check to see if one of the columns is blank (empty string)
        if not eps:
            continue

        # convert to a decimal from a string
        eps = float(eps)

        # check to see if the eps forecast is increasing each year
        if eps < previous_eps:
            return False

        previous_eps = eps

    return True


def earnings_growth_test(ticker):
    """The earnings growth number gives you an idea of how much analysts believe earnings are going to grow per year for the next five years.
    Pass—Give earnings growth a passing score if the Long Term 5-year number is greater than 8%.
    Fail—Give earnings growth a failing score if the Long Term 5-year number is less than 8%."""

    est_eps_growth = pull_earnings_growth(ticker)
    est_eps_growth = float(est_eps_growth[:-1])

    if est_eps_growth < 8.0:
        return False

    return True


def peg_ratio_test(ticker):
    """The PEG ratio is similar to the Price to Earnings (P/E) ratio but inclues a growth factor into its calculation.
    PEG is calculated by dividing the stock's P/E ratio by its expected 12 month growth rate.
    Pass—Give the PEG Ratio a passing score if its value is less than 1.0.
    Fail—Give the PEG Ratio a failing score if its value is greater than 1.0."""

    peg_ratio = float(pull_peg_ratio(ticker))
    if peg_ratio > 1.0:
        return False


    return True


def industry_earnings_test(ticker):
    pass


def days_to_cover_test(ticker):
    """Give Days to Cover a passing score if the number of days is less than 2 days."""

    days_to_cover = pull_days_to_cover(ticker)
    if float(days_to_cover) >= 2:
        return False

    return True


def insider_trading_test(ticker):
    """Give Insider Trading a passing score if the net activity for the past 3 months has been positive"""

    net_activity = pull_recent_net_insider_trading(ticker)

    if '(' in net_activity:
        return False

    return True


def weighted_alpha_test(ticker):
    pass
