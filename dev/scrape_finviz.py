import requests
from bs4 import BeautifulSoup
from io import StringIO
from collections import OrderedDict
import pandas as pd
import codecs, prettytable
from time import sleep


def scrape_finviz(ticker):
    """Scrapes all information about an individual stock from Finviz and writes it to a file labeled finviz_output.txt"""

    # creating a file to record scraped information
    # use utf-8 to handle printing arrows to represent price rating changes
    file = codecs.open('output\\finviz_output.txt', 'w', encoding='utf-8')

    chart_link = pull_chart(ticker)
    file.write('Link to chart image: http://www.finviz.com/' + chart_link + '\n')


    company_basics = pull_company_basics(ticker)
    file.write('\nCompany name: ' + company_basics['name'] + '\n')
    file.write('Company website: ' + company_basics['website'] + '\n')
    file.write('Exchange traded on: ' + company_basics['exchange'] + '\n')
    file.write('\nQuick bio: ' + company_basics['bio'] + '\n')
    file.write('\nCountry: ' + company_basics['country'] + '\n')
    file.write('Sector: ' + company_basics['sector'] + '\n')
    file.write('Industry: ' + company_basics['industry'] + '\n')


    companies_in_industry_df = pull_companies_in_industry(ticker)
    file.write('\nCompanies in Industry:\n')
    file.write(prettify_dataframe(companies_in_industry_df))


    financial_stats = pull_financial_table(ticker)
    file.write('\n\nFinancial stats:')
    # Write out the key, value pairs in the dictionary
    for item in financial_stats.items():
        file.write('\n' + str(item))


    ratings_df = pull_ratings(ticker)
    file.write('\n\nAnalysts ratings: \n')
    if ratings_df is None:
        file.write('No analyst ratings found.')
    else:
        file.write(prettify_dataframe(ratings_df))


    news_articles_df = pull_news_table(ticker)
    #print(news_articles_df)
    file.write('\n\nMost recent news articles:\n')
    if news_articles_df is None:
        file.write('No news found.')
    else:
        file.write(prettify_dataframe(news_articles_df))


    insider_trading_df = pull_insider_trading(ticker)
    file.write('\n\nInsider Trading: \n')
    if insider_trading_df is None:
        file.write('No insider trading found.')
    else:
        file.write(prettify_dataframe(insider_trading_df))


    file.close()

    # print file to terminal
    file = codecs.open('output\\finviz_output.txt', 'r', encoding='utf-8')
    print(file.read())

def get_soup(url):
    sleep(5)
    print('Slept for two seconds')
    print('Going to URL: %s' % url)
    res = requests.get(url)

    try:
        res.raise_for_status()
    except Exception as e:
        print('There was a problem getting soup: %s' % e)
        exit()

    # exampleFile = open('Stock Quote NFLX Netflix, Inc..html')
    # soup = BeautifulSoup(exampleFile, "html.parser")
    return BeautifulSoup(res.text, "html.parser")


def prettify_dataframe(df):
    """small trick to convert the dataframe to an in-memory csv file and have prettytable read it."""

    df_output = StringIO()
    # sometimes the sep seems to need to be a space, other times a comma. Need to investigate.
    df.to_csv(df_output, sep=' ', index=None, header=True)
    df_output.seek(0)

    # prettytable reads from the in-memory csv then is converted to a string
    pt = str(prettytable.from_csv(df_output))
    return pt


def finviz_num_conversion(finviz_val):
    """Takes an abreviated value in the finviz financial table and converts it to its appropriate decimal form"""

    THOUSAND = 'T'
    MILLION = 'M'
    BILLION = 'B'

    try:
        # check the monetary abbreviation
        abv = finviz_val[-1]
        num = float(finviz_val[:-1])

        if abv is '%':
            num = num / 100.0

        if abv == THOUSAND:
            num *= 1000

        if abv == MILLION:
            num *= 1000000

        if abv == BILLION:
            num *= 1000000000
    except Exception as e:
        print('The finviz entry was not a number or percent: %s' % e)
        exit()

    return num





def pull_chart(ticker):
    """Returns a string url link for the stock chart of the ticker"""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Finds the section that contains the image, then pulls the image line
    image_section = soup.find('td', {'align': 'center', 'valign': 'top'}).find('img')
    # Pulls image link
    chart = str(image_section.get('src'))
    return chart



def pull_company_basics(ticker):
    """Returns a dict containing the company name, bio, website, country, sector, and industry it is part of."""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    company_basics = OrderedDict()

    # Pulls the quick bio of the company
    company_basics['bio'] = soup.find('td', {'class': 'fullview-profile'}).get_text()

    # Pulls overview section containing most of the desired information
    overview_section = soup.find('table', {'class' : 'fullview-title'})

    # Pulling the exchange the company's stock is traded on and strip brackets on either side
    stock_exchange = overview_section.find_all('td', {'align': 'center'})[0].find('span').get_text()
    company_basics['exchange'] = stock_exchange[1:-1]
    # Pulling name of the company associated with the ticker
    company_basics['name'] = overview_section.find_all('td', {'align': 'center'})[1].get_text()
    # Pulling the company website
    company_basics['website'] = overview_section.find_all('td', {'align': 'center'})[1].find('a').get('href')

    # Pulling to the section which has the sector, insustry, and country the company is part of
    market_section = overview_section.find('td', {'class' : 'fullview-links'})
    # Pulling sector company is part of
    company_basics['sector'] = market_section.find_all('a')[0].get_text()
    # Pulling industry company is part of
    company_basics['industry'] = market_section.find_all('a')[1].get_text()
    # Pulling out country company is home to
    company_basics['country'] = market_section.find_all('a')[2].get_text()

    return company_basics


def pull_companies_in_industry(ticker):
    """Returns a dataframe containing all of the companies that make up the industry of a particular ticker and a link to each of them."""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Pulling to the section which has the sector, insustry, and country the company is part of
    market_section = soup.find('table', {'class': 'fullview-title'}).find('td', {'class': 'fullview-links'})

    # Pulling out the partial industry link and making the full link
    industry_link = 'http://www.finviz.com/' + str(market_section.find_all('a')[1].get('href'))

    # Initialized dataframe and set schema for table of all companies in the same industry as the specified ticker
    companies_df = pd.DataFrame(columns=['Ticker', 'Company Name', 'Finviz Link'])

    i = 0
    while True:
        #print('\nCurrent link: ' + str(industry_link))
        soup = get_soup(industry_link)

        # Pull out each row from the table containing all of the companies in the given industry
        company_rows = soup.find_all('tr', {'valign' : 'top'})

        # Remove an extra section of html that was not part of the rows
        company_rows = company_rows[1:]

        # Pull out company ticker and company name for each company row
        for row in company_rows:
            sections = row.find_all('td')
            ticker = sections[1].get_text()
            company_link = 'http://www.finviz.com/' + str(sections[1].find('a').get('href'))
            company_name = sections[2].get_text()

            # Create a tuple containing ticker and company name, and company link
            tup = (ticker, company_name, company_link)

            # Add tuple to the list of competitors
            companies_df.loc[i] = tup
            i += 1

        # Find section of html that contains the link to the next page, if there is a next page
        next_page_section = soup.find('td', {'class' : 'body-table'})

        # Number of page buttons contained in the next page section (indicating that there is another page with more information)
        buttons_present = next_page_section.find_all(['a', 'b'])

        #If there is more than one page present
        if len(buttons_present) > 1:
            # If the last button is the next button (indicating there is another page)
            if buttons_present[-1].get_text() == 'next':
                urls = []
                for button in buttons_present:
                    # Get the url links among the buttons
                    if button.get('href') is not None:
                        urls.append(button.get('href'))
                # Create the full link to the next page with the last button's url link (the next button)
                next_page_link = 'http://www.finviz.com/' + str(urls[-1])

            else: # Else this is the last page
                break

        else:   # Else this is the only page with companies in the industry
            break

        industry_link = next_page_link

    return companies_df


def pull_financial_table(ticker):
    """Returns a dictionary containing various financial information/statistics about the ticker"""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Create dictionary to hold the values of the finviz table
    finviz_table = OrderedDict()

    #Pull financial statistics table for the ticker
    financial_table = soup.find("table", {'class' : 'snapshot-table2'})

    # Break down the table by rows
    rows = financial_table.find_all('tr')

    for row in rows:
        # Pull out individual items (a key or a value)
        items = row.find_all('td')

        # iterate i up to a value which is the number of items, and increase i by 2 each time.
        for i in range(0,len(items),2):
            key = items[i].get_text()
            value = items[i+1].get_text()
            finviz_table[key] = value

    return finviz_table


def pull_ratings(ticker):
    """Returns a dataframe containing analyst ratings and opinions about the particular ticker.
    If there are no analyst ratings present, it will return None."""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Pull the html section containing all of the ratings
    ratings_section = soup.find('table', {'class' : 'fullview-ratings-outer'})

    # If none are present, return
    if ratings_section is None:
        return None

    # Pull out individual rows of ratings
    ratings = ratings_section.find_all('td', {'class' : 'fullview-ratings-inner'})

    # Initialize dataframe and set schema
    ratings_df = pd.DataFrame(columns=['Date', 'Status', 'Author', 'Opinion', 'Price'])

    # index to add to dataframe
    i = 0
    for rating in ratings:
        # Pulls out individual column sections of the row
        sections = rating.find_all('td')

        # have variable column to determine which variable needs to be assigned the string based on the stage of parsing
        column = 0
        for section in sections:
            if column == 0:
                date =  section.get_text()
            if column == 1:
                status =  section.get_text()
            if column == 2:
                author = section.get_text()
            if column == 3:
                opinion = section.get_text()
            if column == 4:
                price = section.get_text()

            column += 1

        # compile the information into a duple so it can be placed into a dataframe.
        tup = (date, status, author, opinion, price)
        ratings_df.loc[i] = tup
        i += 1

    return ratings_df


def pull_news_table(ticker):
    """Returns a dataframe of up to 100 of the most recent news articles related to the ticker,
    they are in order of date and time published. If there are no analyst ratings present, it will return None."""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Initialize dataframe and set schema for the news articles
    articles_df = pd.DataFrame(columns=['Timestamp', 'Title', 'Source', 'Link'])

    # Pull the html section containg all of the ratings
    news_section = soup.find('table', {'class' : 'fullview-news-outer'})

    # If none are present, return
    if news_section is None:
        return None

    articles = news_section.find_all('tr')

    i = 0
    for article in articles:
        #pulling article timestamp and trimming the spaces at the end
        article_timestamp = article.find('td').get_text()[:-2]

        # if there is only the time listed and not the date,
        # then take the last date listed and use that
        if len(article_timestamp) < 10:
            article_timestamp = article_date + article_timestamp
            article_link = article.find('td', {'align': 'left'}).find('a').get('href')
            article_title = article.find('td', {'align': 'left'}).find('a').get_text()
            article_source = article.find('td', {'align': 'left'}).find('span').get_text()
            # trim of the word at before the source name
            if article_source[0:2] == 'at':
                article_source = article_source[3:]

            # compile the information into a tuple so it can be placed into a dataframe.
            tup = (article_timestamp, article_title, article_source, article_link)
            articles_df.loc[i] = tup
            i += 1
            continue

        article_date = article_timestamp[0:10]
        article_link = article.find('td', {'align' : 'left'}).find('a').get('href')
        article_title = article.find('td', {'align' : 'left'}).find('a').get_text()
        article_source = article.find('td', {'align': 'left'}).find('span').get_text()
        # trim of the word at before the source name
        if article_source[0:2] == 'at':
            article_source = article_source[3:]

        # compile the information into a tuple so it can be placed into a dataframe.
        tup = (article_timestamp, article_title, article_source, article_link)
        articles_df.loc[i] = tup
        i += 1

    return articles_df


def pull_insider_trading(ticker):
    """Returns a dataframe of up to 100 of the most recent insider trading activity that has occurred
    among executives/board members of the company"""

    soup = get_soup('http://www.finviz.com/quote.ashx?t=' + ticker)

    # Pull the html section containing all of the ratings
    insider_section = soup.find('table', {'class' : 'body-table'})

    # If none are present, return
    if insider_section is None:
        return None

    # Pull each row of the insider trading section, the first result is the schema for the website so it is dropped.
    insider_rows = insider_section.find_all('tr')
    insider_rows = insider_rows[1:]

    # Initialize dataframe and set schema
    insider_trading_df = pd.DataFrame(columns=['Insider Trading', 'Insider Trading Link', 'Relationship',
                                               'Date','Transaction', 'Cost', '#Shares', 'Value ($)',
                                               '#Shares Total', 'SEC form date', 'SEC form link' ])
    # index to add to dataframe
    i = 0
    for row in insider_rows:
        # Pull out the link to the history of the person who did the insider trading (first link found)
        insider_trading_link = 'http://www.finviz.com/' + str(row.find_all('a' , {'class' : 'tab-link'})[0].get('href'))

        # Pull out the SEC form 4 showing the insider trading transaction (second link found)
        sec_link = row.find_all('a', {'class' : 'tab-link'})[1].get('href')

        # Pulls out individual column sections of the row
        sections = row.find_all('td')

        # have variable column to determine which variable needs to be assigned the string based on the stage of parsing
        column = 0
        for section in sections:
            if column == 0:
                insider_trading = section.get_text()
            if column == 1:
                relationship = section.get_text()
            if column == 2:
                date = section.get_text()
            if column == 3:
                transaction = section.get_text()
            if column == 4:
                cost = section.get_text()
            if column == 5:
                num_shares = section.get_text()
            if column == 6:
                value = section.get_text()
            if column == 7:
                num_shares_total = section.get_text()
            if column == 8:
                sec_form_date = section.get_text()

            column += 1

        # compile the information into a duple so it can be placed into a dataframe.
        tup = (insider_trading, insider_trading_link, relationship, date, transaction,
               cost, num_shares, value, num_shares_total, sec_form_date, sec_link)
        insider_trading_df.loc[i] = tup
        i += 1

    return insider_trading_df
