import codecs
from dev.scrape_finviz import scrape_finviz
from dev.scrape_finviz2 import FinvizInfo
from dev.scrape_nasdaq_dozen import compute_nasdaq_dozen


# YOU NEED TO SEE WHICH SOURCES OF DATA ARE THE MOST ACCURATE.
# EX - PEG ratio for netflix on finviz is 4.18 vs 5.61 on NASDAQ/Zachs.



if __name__ == '__main__':
    ticker = input('Enter ticker you want to scrape: ')
    #scrape_finviz(ticker)
    compute_nasdaq_dozen(ticker)
    #
    # finviz_ticker = FinvizInfo(ticker)
    #
    # # creating a file to record scraped finviz information
    # # use utf-8 to handle printing arrows to represent price rating changes
    # file = codecs.open('output\\finviz_output.txt', 'w', encoding='utf-8')
    # finviz_ticker.write_to_file(file)
    #
    # # print file to terminal
    # print(codecs.open('output\\finviz_output.txt', 'r', encoding='utf-8').read())
    #
    # print('\ndone')


# url = 'http://www.reuters.com/finance/stocks/financialHighlights?symbol=NFLX'
# url = 'http://www.nasdaq.com/symbol/wmt/revenue-eps'
# url = 'http://fundamentals.nasdaq.com/redpage.asp?selected=WMT&market=NYSE&LogoPath=&coname=Wal-Mart+Stores%2c+Inc'
# url = 'http://nostarch.com'
