import codecs
from collections import OrderedDict

import pandas as pd

from dev.util import get_soup, finviz_num_conversion, nasdaq_revenue_conversion


# import quandl
#
# # quandl API Key so that we can do more than 50 queries per day.
# quandl.ApiConfig.api_key = "#Xe5KN6ushjdS9WBC-iwT"

# http://www.nasdaq.com/investing/dozen/revenue.aspx
# http://www.nasdaq.com/symbol/nflx/revenue-eps






# need to finish RoE test (where you just access the column by column name)




def compute_nasdaq_dozen(ticker):

    file = codecs.open('output\\nasdaq_dozen.txt', 'w', encoding='utf-8')
    # nasdaq_dozen_tests = OrderedDict()
    #
    # nasdaq_dozen_tests['revenue'] = revenue_test(ticker)
    # nasdaq_dozen_tests['earnings_per_share'] = eps_test(ticker)
    # nasdaq_dozen_tests['return_on_equity'] = roe_test(ticker)
    # nasdaq_dozen_tests['earnings_surprise'] = earnings_surprise_test(ticker)
    # nasdaq_dozen_tests['earnings_forecast'] = earnings_forecast_test(ticker)
    #
    # nasdaq_dozen_tests['earnings_growth'] = earnings_growth_test(ticker)
    # nasdaq_dozen_tests['peg_ratio'] = peg_ratio_test(ticker)
    # nasdaq_dozen_tests['days_to_cover'] = days_to_cover_test(ticker)
    # nasdaq_dozen_tests['insider_trading'] = insider_trading_test(ticker)
    #
    #
    #
    #
    #
    # for key, value in nasdaq_dozen_tests.items():
    #     print(key + ': %s' % value)

    # print()
    #
    # revenue_df = pull_revenue(ticker)
    # file.write('\nRevenue:\n')
    # file.write(prettify_dataframe(revenue_df))
    #
    # eps_df = pull_eps(ticker)
    # file.write('\n\nEPS:\n')
    # file.write(prettify_dataframe(eps_df))
    #
    # financial_ratios_df = pull_financial_ratios(ticker)
    # file.write('\n\nFinancial Ratios:\n')
    # file.write(prettify_dataframe(financial_ratios_df))
    #
    # surprises_df = pull_quart_earning_surprises(ticker)
    # file.write('\n\nEarnings Surprises:\n')
    # file.write(prettify_dataframe(surprises_df))
    #
    # eps_forcasts_df = pull_yearly_eps_forecast(ticker)
    # file.write('\n\nEPS Forecasts:\n')
    # file.write(prettify_dataframe(eps_forcasts_df))
    #
    # exp_5yr_eps_growth = pull_earnings_growth(ticker)
    # file.write('\n\nEPS Growth: ' + exp_5yr_eps_growth)
    #
    # peg_ratio = pull_peg_ratio(ticker)
    # file.write('\n\nPEG Ratio: ' + peg_ratio)
    #
    # days_to_cover = pull_days_to_cover(ticker)
    # file.write('\n\nDays to Cover: %s' % days_to_cover)

    net_insider_trading = pull_recent_net_insider_trading(ticker)
    file.write('\n\n3 Month Net Insider Trading: ' + net_insider_trading)






    # zacks_rank = pull_zacks_rank(ticker)
    # if zacks_rank == 1:
    #     zacks_rank = '%s - Strong Buy' % zacks_rank
    # if zacks_rank == 2:
    #     zacks_rank = '%s  - Buy' % zacks_rank
    # if zacks_rank == 3:
    #     zacks_rank = '%s - Hold' % zacks_rank
    # if zacks_rank == 4:
    #     zacks_rank = '%s - Sell' % zacks_rank
    # if zacks_rank == 5:
    #     zacks_rank = '%s - Strong Sell' % zacks_rank
    # file.write('\n\nZacks Rank: ' + zacks_rank)
    #
    #
    # style_scores = pull_zacks_style_scores(ticker)
    # file.write('\n\nZacks Style Scores:')
    # for key, value in style_scores.items():
    #     file.write('\n' + key + ': %s' % value)


    file.close()

    # print file to terminal
    file = codecs.open('output\\nasdaq_dozen.txt', 'r', encoding='utf-8')
    print(file.read())


def pull_zacks_rank(ticker):
    """This is Zack's short term rating system that serves as a timeliness indicator for stocks over the next 1 to 3 months.
    Since 1988, Zacks #1 Rank stocks have generated an average annual return of 26% versus just 2.29% for the Zacks #5 Rank stocks.
    This means that the Zacks Rank will help you identify the winners and avoid the losers in all market conditions."""
    #https://www.zacks.com/stock/research/CVX/brokerage-recommendations
    # 1 - Strong Buy
    # 2 - Buy
    # 3 - Hold
    # 4 - Sell
    # 5 - Strong Sell

    #file = open("//Users/danielduncan/Downloads/PEG_ratio.htm")
    #soup = BeautifulSoup(file, "html.parser")

    soup = get_soup("https://www.zacks.com/stock/quote/" + ticker)

    premium_research_table = soup.find('section', {'id': 'premium_research'})
    zacks_rank = int(premium_research_table.find('tr').find('td').find('span').getText())

    return zacks_rank


def pull_zacks_style_scores(ticker):
    """The Style Scores are a complementary set of indicators to use alongside the Zacks Rank.
    It allows the user to better focus on the stocks that are the best fit for his or her personal trading style.
    The scores are based on the trading styles of Value, Growth, and Momentum.
    There's also a VGM Score ('V' for Value, 'G' for Growth and 'M' for Momentum),
    which combines the weighted average of the individual style scores into one score.

    This will return a disctionary of the different style scores"""


    #file = open("//Users/danielduncan/Downloads/PEG_ratio.htm")
    #soup = BeautifulSoup(file, "html.parser")

    soup = get_soup("https://www.zacks.com/stock/quote/" + ticker)

    style_scores = OrderedDict()

    premium_research_table = soup.find('section', {'id': 'premium_research'})
    style_scores_section = premium_research_table.findAll('tr')[3].find('p')

    style_scores['Value'] = style_scores_section.findAll('span')[0].getText()
    style_scores['Growth'] = style_scores_section.findAll('span')[1].getText()
    style_scores['Momentum'] = style_scores_section.findAll('span')[2].getText()
    style_scores['VGM'] = style_scores_section.findAll('span')[3].getText()

    return style_scores



def pull_revenue(ticker):
    """Returns a dataframe containing the Revenue for the past three fiscal years"""

    # chromedriver must be installed and added to system PATH for this to run
    # browser = webdriver.Chrome()
    # browser.get('http://www.nasdaq.com/symbol/' + ticker + '/revenue-eps')
    # #print(browser.page_source)
    #
    # iframe = browser.find_element_by_id('frmMain')
    # browser.switch_to.default_content()
    # browser.switch_to.frame(iframe)
    # print(browser.page_source)

    soup = get_soup('http://www.nasdaq.com/symbol/' + ticker + '/revenue-eps')

    revenue_iframe_link = soup.find('iframe', {'id' : 'frmMain'}).get('src')
    #print(revenue_iframe_link)

    soup = get_soup(revenue_iframe_link)

    revenue_table = soup.find('table', {'class' : 'ipos'})
    schema_row = revenue_table.find('tr', {'bgcolor' : '#99cbe5'})
    schema_cols = schema_row.findAll('td', {'nowrap' : 'nowrap'})

    # add the table headers to the dataframe's schema list
    df_schema = list()
    for col in schema_cols:
        df_schema.append(col.getText())

    # Initialized dataframe and set schema
    revenue_df = pd.DataFrame(columns=df_schema)

    # getting the financial quarters in the table
    financial_quarters = list()
    all_rows = revenue_table.findAll('tr')
    for row in all_rows:
        financial_quarter = row.find('td', {'class': 'body1'})
        if financial_quarter is None or financial_quarter.b is None:
            continue
        financial_quarters.append(financial_quarter.b.string)


    revenue_rows = revenue_table.findAll('tr', {'bgcolor' : '#eeeeee'})
    # trick to skip every other element in the list (every second element is dividends, we only want revenue)
    revenue_rows = revenue_rows[::2]

    # getting the revenues for each row in the table online and creating our own row and inserting it into the dataframe
    i = 0
    for row in revenue_rows:
        revenue_df_row = list()
        revenue_df_row.append(financial_quarters[i])
        revenue_cols = row.findAll('td', {'align': 'center'})
        for col in revenue_cols:
            revenue_df_row.append(col.getText())

        revenue_df.loc[i] = revenue_df_row
        i += 1

    return revenue_df


def pull_eps(ticker):
    """Returns a dataframe containing the EPS for the past three fiscal years"""

    soup = get_soup('http://www.nasdaq.com/symbol/' + ticker + '/revenue-eps')

    revenue_iframe_link = soup.find('iframe', {'id': 'frmMain'}).get('src')
    #print(revenue_iframe_link)

    soup = get_soup(revenue_iframe_link)

    eps_table = soup.find('table', {'class': 'ipos'})
    schema_row = eps_table.find('tr', {'bgcolor': '#99cbe5'})
    schema_cols = schema_row.findAll('td', {'nowrap': 'nowrap'})

    # add the table headers to the dataframe's schema list
    df_schema = list()
    for col in schema_cols:
        df_schema.append(col.getText())

    # Initialized dataframe and set schema
    eps_df = pd.DataFrame(columns=df_schema)

    # getting the financial quarters in the table
    financial_quarters = list()
    all_rows = eps_table.findAll('tr')
    for row in all_rows:
        financial_quarter = row.find('td', {'class': 'body1'})
        if financial_quarter is None or financial_quarter.b is None:
            continue
        financial_quarters.append(financial_quarter.b.string)

    eps_rows = eps_table.findAll('tr', {'bgcolor': '#ffffff'})

    # getting the revenues for each row in the table online and creating our own row and inserting it into the dataframe
    i = 0
    for row in eps_rows:
        eps_df_row = list()
        eps_df_row.append(financial_quarters[i])
        eps_cols = row.findAll('td', {'align': 'center'})
        for col in eps_cols:
            eps_df_row.append(col.getText())

        eps_df.loc[i] = eps_df_row
        i += 1

    return eps_df


def pull_financial_ratios(ticker):

    soup = get_soup('http://www.nasdaq.com/symbol/' + ticker + '/financials?query=ratios')

    ratios_table = soup.find('div', {'id' : 'financials-iframe-wrap'}).find('table')

    schema_cols = ratios_table.find('thead').findAll('th')

    # # add the table headers to the dataframe's schema list
    df_schema = list()
    df_schema.append('Ratio Type')
    # skip the first two schema headers (not needed)
    i = 0
    for col in schema_cols:
        if i < 2:
            i += 1
            continue
        df_schema.append(col.getText())
        i += 1

    # Initialized dataframe and set schema
    financial_ratios_df = pd.DataFrame(columns=df_schema)

    i = 0
    # Beautiful Soup will examine all the descendants of a tag: its children, its children’s children, and so on.
    # If you only want Beautiful Soup to consider direct children, you can pass in recursive=False.
    ratio_rows = ratios_table.findAll('tr', recursive = False)
    for row in ratio_rows:

        ratio_df_row = list()

        # skip these rows in the table (they are sub headers for some of the ratios in the big table)
        if "category TalignL" in str(row):
            continue

        ratio_type = row.find('th').getText()
        ratio_df_row.append(ratio_type)

        # only want the direct children of the tag
        ratio_percentage_cols = row.findAll('td', recursive=False)
        for col in ratio_percentage_cols:

            if "td_genTable" in str(col):
                continue

            ratio_df_row.append(col.getText())

        financial_ratios_df.loc[i] = ratio_df_row
        i += 1

    return financial_ratios_df



def pull_analyst_recommendations(ticker):
    """the average brokerage recommendation (ABR) is the calculated average of the actual
    recommendations (strong buy, hold, sell etc) made by the brokerage firms for a given stock."""
    # https://www.zacks.com/stock/research/CVX/brokerage-recommendations
    # FOR ANALYST RATINGS PART
    # Mean Recomendation Conversion Table
    # 1.00 thru 1.24 = Buy (2.06 zachs)
    # 1.25 thru 1.74 = Overweight (2.06 - 2.9)
    # 1.75 thru 2.24 = Hold (2.9 - 3.733333333)
    # 2.25 thru 2.74 = Underweight
    # 2.75 thru 3.00 = Sell
    pass


def pull_quart_earning_surprises(ticker):
    """Returns a dataframe containing the Fiscal Quarter End date, date reported, actual EPS, estimated EPS,
    and the EPS surprise percentage that the actual EPS differed from the estimated EPS forecast"""

    soup = get_soup('http://www.nasdaq.com/symbol/' + ticker + '/earnings-surprise')

    # Initialized dataframe and set schema
    surprises_df = pd.DataFrame(columns=['Fiscal Quarter End', 'Date Reported', 'Actual EPS', 'Estimated EPS', 'Percent Surprise'])

    i = 0
    surprise_table = soup.find('div', {'class': 'genTable'})
    for row in surprise_table.findAll('tr'):

        cols = row.findAll('td')
        if len(cols) == 5:

            quarter = str(cols[0].getText())
            if quarter != 'FiscalQuarter End':
                date = str(cols[1].getText())
                actual_eps = str(cols[2].getText())
                estimated_eps = str(cols[3].getText())
                surprise = str(cols[4].getText())

                # add the values into the surprises dataframe table
                tup = (quarter, date, actual_eps, estimated_eps, surprise)
                surprises_df.loc[i] = tup
                i += 1

    return surprises_df


def pull_yearly_eps_forecast(ticker):
    """Returns a dataframe containing the year of the eps forecast, the consensus EPS forecast,
     the high EPS forecast, the low EPS forecast, and the number of estimates that were made"""

    soup = get_soup("http://www.nasdaq.com/symbol/" + ticker + "/earnings-forecast")

    # Initialized dataframe and set schema
    forecasts_df = pd.DataFrame(columns=['Year', 'Consensus EPS Forecasts', 'High EPS Forecasts', 'Low EPS Forecasts', 'Number of Estimates'])

    forecast_table = soup.find('div', {'class': 'genTable'}).find("table")
    rows = forecast_table.findAll("tr")
    i = 0
    for row in rows:
        cols = row.findAll("td")

        #If the first row is the header schema for the table, skip it
        if not cols:
            continue

        year = cols[0].getText()
        year = year[len(year)-4:]
        consensus_eps_forecast = str(cols[1].getText())
        high_eps_forecast = str(cols[2].getText())
        low_eps_forecast = str(cols[3].getText())
        num_estimates = str(cols[4].getText())

        # add the values into the forecasts dataframe table
        tup = (year, consensus_eps_forecast, high_eps_forecast, low_eps_forecast, num_estimates)
        forecasts_df.loc[i] = tup
        i += 1

    return forecasts_df

# NOT FINISHED NEED TO FIX - DOESNT WORK FOR ALL (ex COp and AVXL)
def pull_earnings_growth(ticker):
    """Returns the 3-5 year expected EPS growth percentage for the given ticker"""

    #file = open("//Users/danielduncan/Downloads/PEG_ratio.htm")
    #soup = BeautifulSoup(file, "html.parser")

    soup = get_soup("https://www.zacks.com/stock/quote/" + ticker)

    quote_overviw_table = soup.find('div', {'class': 'quote_body'}).find('section', {'id': 'quote_overview'})
    key_earnings__data_column = quote_overviw_table.find('section', {'id': 'stock_key_earnings'}).find('table', {
        'class': 'abut_bottom'})

    exp_5yr_eps_growth = key_earnings__data_column.findAll('tr')[-1].findAll('a')[1].getText()

    return exp_5yr_eps_growth


def pull_peg_ratio(ticker):
    """Returns the peg_ratio value for the given ticker"""

    #file = open("//Users/danielduncan/Downloads/PEG_ratio.htm")
    #soup = BeautifulSoup(file, "html.parser")

    soup = get_soup("https://www.zacks.com/stock/quote/" + ticker)

    quote_overviw_table = soup.find('div', {'class' : 'quote_body'}).find('section', {'id' : 'quote_overview'})
    key_earnings__data_column = quote_overviw_table.find('section', {'id' : 'stock_key_earnings'}).find('table', {'class' : 'abut_bottom'})

    peg_ratio = key_earnings__data_column.findAll('tr')[1].findAll('td')[1].getText()

    return peg_ratio


def pull_industry_earnings(ticker):
    pass


def pull_days_to_cover(ticker):
    """Returns the number of days to cover for a specific stock.

    Days to cover is the number of days based on the average trading volume of the
    stock that it would take all short sellers to cover their short positions.
    For instance, if a stock has a short interest of 20 million shares and an average trading volume
    of 10 million shares, days to cover would be two days (20 million / 10 million = 2 days)."""


    # soup = get_soup("http://www.nasdaq.com/symbol/"+ticker+"/short-interest")
    #
    # table = soup.find('div', {'class' : 'genTable'}).find('table')
    # days_to_cover = table.find('tbody').find('tr').findAll('td')[-1].getText()
    # print(days_to_cover)


    finviz_table = pull_financial_table(ticker)

    shs_outstanding = finviz_num_conversion(finviz_table['Shs Outstand'])
    short_float_p = finviz_num_conversion(finviz_table['Short Float'])
    shs_short = shs_outstanding * short_float_p

    avg_vol = finviz_num_conversion(finviz_table['Avg Volume'])
    days_to_cover = shs_short / avg_vol

    return days_to_cover


def pull_recent_net_insider_trading(ticker):
    """Pulls the net insider trading activity for the past three months"""

    #file = open("//Users/danielduncan/Downloads/NFLX:insider_activity.html")
    #soup = BeautifulSoup(file, "html.parser")

    soup = get_soup("http://www.nasdaq.com/symbol/" + ticker + "/insider-trades")

    table = soup.findAll('div', {'class' : 'infoTable'})[-1]
    net_activity = table.findAll('tr')[-1].find('td').getText()

    return net_activity


def pull_weighted_alpha(ticker):
    pass









def revenue_test(ticker):
    """Checks to see if revenue is increasing. First check the annual totals, but if the most recent fiscal year is
    incomplete, then compare the most recent quarter with the same quarter in the previous year."""

    revenue_df = pull_revenue(ticker)
    if revenue_df.empty:
        return False

    curr_fiscal_year_complete = True
    most_recent_quater_loc = 0

    # check to see if the most recent fiscal year is complete and revenue is recorded for each quarter (no blanks).
    # if not complete, record the location most recent quarter
    i = 0
    for quart_rev in revenue_df.iloc[:,1]:
        #print(":".join("{:02x}".format(ord(c)) for c in quart_rev))
        if (not quart_rev) or (quart_rev == chr(0xa0) or (quart_rev == ' ')):
            curr_fiscal_year_complete = False
            most_recent_quater_loc = i-1
        i += 1

    # if all years are complete, check to see if each year the revenue is increasing
    if curr_fiscal_year_complete:
        prev_rev = -99999

        # pull the revenue for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_rev in reversed(revenue_df.iloc[-1,1:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if year_rev == chr(0xa0) or year_rev == 'N/A':
                return False

            year_rev = nasdaq_revenue_conversion(year_rev)
            if year_rev < prev_rev:
                return False

            prev_rev = year_rev

    else:
        prev_rev = -99999

        # pull the revenue for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_rev in reversed(revenue_df.iloc[-1, 2:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if year_rev == chr(0xa0) or year_rev == 'N/A':
                return False

            year_rev = nasdaq_revenue_conversion(year_rev)
            if year_rev < prev_rev:
                return False

            prev_rev = year_rev

        prev_rev = -99999
        # pull the revenue for the last listed fiscal quarter and the revenues from previous years for the same quarter
        for quart_rev in reversed(revenue_df.iloc[most_recent_quater_loc, 1:-1]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported revenue or the company is revenue negative)
            if quart_rev == chr(0xa0) or quart_rev == 'N/A':
                return False

            quart_rev = nasdaq_revenue_conversion(quart_rev)
            if quart_rev < prev_rev:
                return False

            prev_rev = quart_rev

    return True


def eps_test(ticker):
    """Checks to see if revenue is increasing. First check the annual totals, but if the most recent fiscal year is
        incomplete, then compare the most recent quarter with the same quarter in the previous year."""

    eps_df = pull_eps(ticker)
    if eps_df.empty:
        return False

    curr_fiscal_year_complete = True
    most_recent_quater_loc = 0

    # check to see if the most recent fiscal year is complete and eps is recorded for each quarter (no blanks).
    # if not complete, record the location most recent quarter
    i = 0
    for quart_eps in eps_df.iloc[:, 1]:
        #print(":".join("{:02x}".format(ord(c)) for c in quart_eps))
        if (not quart_eps) or (quart_eps == chr(0xa0) or (quart_eps == ' ')):
            curr_fiscal_year_complete = False
            most_recent_quater_loc = i - 1
        i += 1

    # # if all years are complete, check to see if each year the eps is increasing
    if curr_fiscal_year_complete:
        prev_eps = -99999

        # pull the eps for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_eps in reversed(eps_df.iloc[-1, 1:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if year_eps == chr(0xa0) or year_eps == 'N/A':
                return False

            year_eps = float(year_eps)

            if year_eps < prev_eps:
                return False

            prev_eps = year_eps

    else:
        prev_eps = -99999

        #pull the eps for each year in the table (exclude first column as it is the fiscal quarter month)
        for year_eps in reversed(eps_df.iloc[-1, 2:]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if year_eps == chr(0xa0) or year_eps == 'N/A':
                return False

            year_eps = float(year_eps)

            if year_eps < prev_eps:
                return False

            prev_eps = year_eps

        prev_eps = -99999
        # pull the eps for the last listed fiscal quarter and the eps' from previous years for the same quarter
        for quart_eps in reversed(eps_df.iloc[most_recent_quater_loc, 1:-1]):

            # if the field is blank or is 'N/A' return false (most likely there is no reported eps or the company is eps negative)
            if quart_eps == chr(0xa0) or quart_eps == 'N/A':
                return False

            # remove the eps release date next to the number and convert it to a float to compare
            quart_eps = float(quart_eps[:quart_eps.index('(')])

            if quart_eps < prev_eps:
                return False

            prev_eps = quart_eps

    return True


def roe_test(ticker):
    """Give ROE a passing score if ROE has been increasing for two consecutive years."""

    financial_ratios_df = pull_financial_ratios(ticker)

    #print(prettify_dataframe(financial_ratios_df))
    # #x = financial_ratios_df.loc["After Tax ROE", "12/31/2015"]
    # x = financial_ratios_df.get_group("After Tax ROE")
    # print(x)

    prev_roe = -99999
    # pull the After Tax Return on Equity (RoE) percentage for each year
    for year_roe in reversed(financial_ratios_df.iloc[-1, 1:]):

        # if the field is blank or is 'N/A' return false
        if year_roe == chr(0xa0) or year_roe == 'N/A':
            return False

        print(year_roe)
        year_roe = float(year_roe[:year_roe.index('%')])
        print(year_roe)

        if year_roe < prev_roe:
            return False

        prev_roe = year_roe

    return True



def analyst_recommendations(ticker):
    pass


def earnings_surprise_test(ticker):
    """Takes the surprises dataframe pulled from NASDAQ and rates if the EPS surprise was a pass or fail.
    If all EPS percent surprises for the last 4 quaters are greater than 0, pass, else, fail. """

    surprises_df = pull_quart_earning_surprises(ticker)
    if surprises_df.empty:
        return False

    for value in surprises_df['Percent Surprise']:

        # check to see if one of the columns is blank (empty string)
        if not value:
            continue

        # convert to a decimal and check if it is positive
        if float(value) < 0:
            return False

    return True


def earnings_forecast_test(ticker):
    """Takes the eps forecasts dataframe pulled from NASDAQ and rates if the earnings forecast was a pass or fail.
    If the consensus EPS forecast is increasing each year give it a pass, else fail. """

    eps_forecasts_df = pull_yearly_eps_forecast(ticker)
    if eps_forecasts_df.empty:
        return False

    previous_eps = -99999
    for eps in eps_forecasts_df['Consensus EPS Forecasts']:

        # check to see if one of the columns is blank (empty string)
        if not eps:
            continue

        # convert to a decimal from a string
        eps = float(eps)

        # check to see if the eps forecast is increasing each year
        if eps < previous_eps:
            return False

        previous_eps = eps

    return True


def earnings_growth_test(ticker):
    """The earnings growth number gives you an idea of how much analysts believe earnings are going to grow per year for the next five years.
    Pass—Give earnings growth a passing score if the Long Term 5-year number is greater than 8%.
    Fail—Give earnings growth a failing score if the Long Term 5-year number is less than 8%."""

    est_eps_growth = pull_earnings_growth(ticker)
    est_eps_growth = float(est_eps_growth[:-1])

    if est_eps_growth < 8.0:
        return False

    return True


def peg_ratio_test(ticker):
    """The PEG ratio is similar to the Price to Earnings (P/E) ratio but inclues a growth factor into its calculation.
    PEG is calculated by dividing the stock's P/E ratio by its expected 12 month growth rate.
    Pass—Give the PEG Ratio a passing score if its value is less than 1.0.
    Fail—Give the PEG Ratio a failing score if its value is greater than 1.0."""

    peg_ratio = float(pull_peg_ratio(ticker))
    if peg_ratio > 1.0:
        return False


    return True


def industry_earnings_test(ticker):
    pass


def days_to_cover_test(ticker):
    """Give Days to Cover a passing score if the number of days is less than 2 days."""

    days_to_cover = pull_days_to_cover(ticker)
    if float(days_to_cover) >= 2:
        return False

    return True


def insider_trading_test(ticker):
    """Give Insider Trading a passing score if the net activity for the past 3 months has been positive"""

    net_activity = pull_recent_net_insider_trading(ticker)

    if '(' in net_activity:
        return False

    return True


def weighted_alpha_test(ticker):
    pass


