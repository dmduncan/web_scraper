class ticker():

    date_pulled = None
    ticker = None
    '''
    Pulled from finviz.com
    '''
    # MAYBE LOOK AT http://www.reuters.com/finance/stocks/financialHighlights?symbol=NFLX.O
    # for titles for the section of financial details

    company_name = None
    #Major index membership
    index = None
    #Market Capitalization
    market_cap = None
    #Income (ttm)
    income = None
    #Revenue
    sales = None
    #Book value per share (mrq)
    book_sh = None
    #Cash per share (mrq)
    cash_sh = None
    #Dividend (annual)
    dividend = None
    #Dividend yield (annual)
    dividend_yield = None
    #Full time Employees
    employees = None
    #Stock has options trading on a market exchange
    optionable = None
    #Stock available to sell short
    shortable = None
    #Analysts' mean recommendation
    recom = None


    #Price-to-earnings (ttm)
    pe = None
    #Forward Price-to-earnings (next fiscal year)
    forward_pe = None
    #Price-to-Earnings-to-Growth
    peg = None
    #Price-to-Sales (ttm)
    ps = None
    #Price-to-Book (mrq)
    pb = None
    #Price to cash per share (mrq)
    pc = None
    #Price to Free Cash Flow (ttm)
    pfcf = None
    #Quick Ratio (mrq)
    quick_ratio = None
    #Current ratio (mrq)
    current_ratio = None
    #Total Debt to Equity (mrq)
    debt_equity = None
    #Long term Debt to Equity (mrq)
    lt_debt_equity = None


    #Diluted EPS (ttm)
    eps = None
    #EPS estimate for next year
    eps_ny = None
    #EPS estimate for next quater
    eps_nq = None
    #EPS Growth this Year (Percentage)
    eps_p_ty = None
    #EPS Growth next Year (Percentage)
    eps_p_ny = None
    #Long term annual growth estimate (5 years) (Percentage)
    eps_p_n5y = None
    #Annual EPS growth past 5 years (Percentage)
    eps_p_p5y = None
    #Annual sales growth past 5 years (Percentage)
    sales_p_p5y = None
    #Quaterly revenue growth (yoy) (Percentage)
    sales_p_q = None
    #Quaterly earnings growth (yoy) (Percentage)
    eps_p_q = None
    #Earnings Date. BMO = Before Market Open AMC = After Market Close
    earnings_date = None


    #Insider Ownership (Percentage)
    insider_own_p = None
    #Insider transactions (6-Month change in Insider Ownership) (Percentage)
    insider_trans_p = None
    #Institutional Ownership (Percentage)
    inst_own_p = None
    #Institutional Transactions (3-month change in institutional ownership) (Percentage)
    inst_trans_p = None
    #Return on assets (ttm) (Percentage)
    roa_p = None
    #Return on equity (ttm) (Percentage)
    roe_p = None
    #Gross margin (ttm) (Percentage)
    gross_margin_p = None
    #Operating Margin (ttm) (Percentage)
    opr_margin_p = None
    #Net Profit Margin (ttm) (Percentage)
    profit_margin_p = None
    #Dividend Payout Ratio (ttm) (Percentage)
    div_payout_p = None



    #Shares outstanding
    shs_outstand = None
    # Shares Float
    shs_float = None
    #Short interest share (Percentage)
    short_float_p = None
    #Short interest ratio
    short_ratio = None
    #Analysts' Mean Target Price
    target_price = None
    #52-Week trading range
    w52_range = None
    #Distance from 52-Week high (Percentage)
    dist_52w_high_p = None
    #Distance from 52-Week low (Percentage)
    dist_52w_low_p = None
    #Relative strength index
    rsi_14 = None
    #Relative Volume
    rel_vol = None
    #Average Volume (3 months)
    avg_vol = None
    #Volume
    vol = None
    #Distance from 20-day Simple Moving Average (Percentage)
    sma20_p = None
    #Distance from 50-day Simple Moving Average (Percentage)
    sma50_p = None
    #Distance from 200-day Simple Moving Average (Percentage)
    sma200_p = None


    #Performance Week (Percentage)
    perf_week_p = None
    #Performance Month (Percentage)
    perf_month_p = None
    #Performance Quarter (Percentage)
    perf_quater_p = None
    #Performance Half Year (Percentage)
    perf_halfyear_p = None
    #Performance Year (Percentage)
    perf_year_p = None
    #Performance Year to Date (Percentage)
    perf_ytd_p = None
    #Beta
    beta = None
    #Avergae True Range
    atr = None
    #Volatility (Week, Month) (Percentage)
    volatility_p = None
    #Previous Close
    prev_close = None
    #Current stock price
    price = None
    #Performance today
    change = None



