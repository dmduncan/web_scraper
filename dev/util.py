import requests, prettytable
from bs4 import BeautifulSoup
from io import StringIO
from time import sleep

def get_soup(url):
    sleep(5)
    print('Slept for five seconds')
    print('Going to URL: %s' % url)
    res = requests.get(url)

    try:
        res.raise_for_status()
    except Exception as e:
        print('There was a problem getting soup for %s: %s' % (url, e))
        exit()

    # exampleFile = open('Stock Quote NFLX Netflix, Inc..html')
    # soup = BeautifulSoup(exampleFile, "html.parser")
    return BeautifulSoup(res.text, "html.parser")

def prettify_dataframe(df):
    """small trick to convert the dataframe to an in-memory csv file and have prettytable read it."""

    # This try/catch was made for a problem encountered in the news articles dataframe.
    # The separator value a lot of the time is in the URL so it is getting separated there
    # and crashing so we try every ascii combination possible in hope one isn't in any of
    # the URLs
    i = 0
    pt = None
    while pt is None:
        try:
            df_output = StringIO()
            # sometimes the sep seems to need to be a space, other times a comma. Need to investigate.
            df.to_csv(df_output, sep=chr(i), index=None, header=True)
            df_output.seek(0)

            # prettytable reads from the in-memory csv then is converted to a string
            pt = str(prettytable.from_csv(df_output))
        except Exception:
            i += 1
    print('I was : %s' % i)
    return pt


def finviz_num_conversion(val):
    """Takes an abreviated value in the finviz financial table and converts it to its appropriate decimal form"""

    THOUSAND = 'T'
    MILLION = 'M'
    BILLION = 'B'

    try:
        # check the monetary abbreviation
        abv = val[-1]
        num = float(val[:-1])

        if abv is '%':
            num = num / 100.0

        if abv == THOUSAND:
            num *= 1000

        if abv == MILLION:
            num *= 1000000

        if abv == BILLION:
            num *= 1000000000
    except Exception as e:
        print('The finviz entry was not a number or percent: %s' % e)
        exit()

    return num


def nasdaq_revenue_conversion(val):
    """Converts string version of a balance sheet number to a float.
    Ex: $6,353(m) converts to 6353000000.0"""

    THOUSAND = 't'
    MILLION = 'm'
    BILLION = 'b'

    # strip off the dollar sign
    num = val[1:]

    # check the monetary abbreviation
    abv = num[-2]

    # strip to just the number and convert to float
    num = float(num[:-3].replace(',' , ''))

    if abv == THOUSAND:
        num *= 1000

    if abv == MILLION:
        num *= 1000000

    if abv == BILLION:
        num *= 1000000000

    return num